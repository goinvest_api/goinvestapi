import urllib2
import time
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
#candlestick cannot be resolved with matplotlib 1.5.0 (deprecated)
#from matplotlib.finance import candlestick
from matplotlib.finance import candlestick_ochl
import matplotlib
import pylab
import psycopg2
matplotlib.rcParams.update({'font.size': 9})

from stockAPI import *




stock = '^STI'
#stock = 'G3B.SI'
#pullData1Y(stock)
plotData(stock)