import urllib2
import time
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
# candlestick cannot be resolved with matplotlib 1.5.0 (deprecated)
# from matplotlib.finance import candlestick
from matplotlib.finance import candlestick_ochl
import matplotlib
import pylab
import psycopg2
from pykalman.standard import KalmanFilter
from numpy import ma, NaN, nan
import math
from numpy.random.mtrand import uniform

def pullDataDB(stock):
    param = (stock,)

    select = "select date, close, high, low, open, volume from stock_data where code = %s order by date ASC"

    conn = psycopg2.connect(database='goinvest', user='postgres', password='G01nvest', host='goinvestweb.cloudapp.net', port=5432)
    cursor = conn.cursor()

    cursor.execute(select, param)
    data = cursor.fetchall()

    cursor.close()
    conn.close()

    fileLine = stock + '.txt'

    saveFile = open(fileLine, 'a')

    for i in range(len(data)):
        eachLine = data[i][0].strftime('%Y%m%d') + ',' + str(data[i][1]) + ',' + str(data[i][2]) + ',' + str(data[i][3]) + ',' + str(data[i][4]) + ',' + str(data[i][5])
        lineToWrite = eachLine + '\n'
        saveFile.write(lineToWrite)
    saveFile.close()

def pullData1Y(stock):
    try:
        fileLine = stock + '.txt'
        urlToVisit = 'http://chartapi.finance.yahoo.com/instrument/1.0/' + stock + '/chartdata;type=quote;range=1y/csv'
        sourceCode = urllib2.urlopen(urlToVisit).read()

        splitSource = sourceCode.split('\n')
        for eachLine in splitSource:
            splitLine = eachLine.split(',')

            if len(splitLine) == 6:
                if 'values' not in eachLine:
                    saveFile = open(fileLine, 'a')
                    lineToWrite = eachLine + '\n'
                    saveFile.write(lineToWrite)
        saveFile.close()

        print 'Pulled', stock
        print 'Sleep for 5 seconds'
        time.sleep(5)
    except Exception, e:
        print 'main loop', str(e)

def plotData(stock):
    try:
        stockFile = stock + '.txt'
        date, closep, highp, lowp, openp, volume = np.loadtxt(stockFile, delimiter=',', unpack=True,
                                                          converters={ 0: mdates.strpdate2num('%Y%m%d')})

        x = 0
        y = len(date)
        candleAr = []
        while x < y:
            appendLine = date[x], openp[x], closep[x], highp[x], lowp[x], volume[x]
            candleAr.append(appendLine)
            x += 1


        # MA5 MA12 MA26
        Av0 = MA(closep, 5)
        Av1 = MA(closep, 12)
        Av2 = MA(closep, 26)

        SP_MA = len(date[26 - 1:])

        # RSI
        rsi = RSI(closep)

        # MACD
        diff, dea, osc = MACD(closep)

        # BOLL
        mu, up, down = BOLL(closep)
        SP_BOLL = len(date[40 - 1:])

        # KDJ
        klist, dlist, jlist = KDJ(highp, lowp, closep)
        SP_KDJ = len(date[9:])

        # KALMAN
        mean, var, modified_data = KALMAN(closep,volume)
        
        # plot data

        unit = 15
        fig = plt.figure()

        ax1 = plt.subplot2grid((unit, 4), (0, 0), rowspan=5, colspan=4)

        # plot candle line
        # candlestick(ax1,candleAr,width=1,colorup='r',colordown='g')
        candlestick_ochl(ax1, candleAr, width=1, colorup='r', colordown='g')

        # plot moving average
        ax1.plot(date[-SP_MA:], Av0[-SP_MA:], 'm')
        ax1.plot(date[-SP_MA:], Av1[-SP_MA:], 'y')
        ax1.plot(date[-SP_MA:], Av2[-SP_MA:], 'b')

        ax1.grid(True)
        ax1.xaxis.set_major_locator(mticker.MaxNLocator(5))
        ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
        plt.ylabel('Stock Price')

        # plot volume
        ax2 = plt.subplot2grid((unit, 4), (5, 0), rowspan=1, colspan=4, sharex=ax1)
        barlist = ax2.bar(date, volume)
        for i in range(len(date)):
            if openp[i] < closep[i]:
                barlist[i].set_color('r')
            else:
                barlist[i].set_color('g')
        ax2.grid(True)
        ax2.axes.yaxis.set_ticklabels([])
        plt.ylabel('Volume')

        # plot RSI
        ax3 = plt.subplot2grid((unit, 4), (6, 0), rowspan=1, colspan=4, sharex=ax1)
        ax3.plot(date, rsi)
        ax3.axhline(70, color='g')
        ax3.axhline(30, color='r')

        # plot BOLL
        ax4 = plt.subplot2grid((unit, 4), (7, 0), rowspan=1, colspan=4, sharex=ax1)
        ax4.plot(date[-SP_BOLL:], mu[-SP_BOLL:], 'b')
        ax4.plot(date[-SP_BOLL:], up[-SP_BOLL:], 'y')
        ax4.plot(date[-SP_BOLL:], down[-SP_BOLL:], 'm')
        ax4.plot(date[-SP_BOLL:], closep[-SP_BOLL:], 'r')

        # plot MACD
        ax5 = plt.subplot2grid((unit, 4), (8, 0), rowspan=1, colspan=4, sharex=ax1)
        ax5.plot(date[-SP_MA:], diff[-SP_MA:], color='b')
        ax5.plot(date[-SP_MA:], dea[-SP_MA:], color='y')
        ax5.fill_between(date[-SP_MA:], osc[-SP_MA:], 0, alpha=0.5, facecolor='c', edgecolor='c')

        # plot KDJ
        ax6 = plt.subplot2grid((unit, 4), (9, 0), rowspan=1, colspan=4, sharex=ax1)
        ax6.plot(date[-SP_KDJ:], klist, color='b')
        ax6.plot(date[-SP_KDJ:], dlist, color='y')
        ax6.plot(date[-SP_KDJ:], jlist, color='m')
        ax6.axhline(20, color='r')
        ax6.axhline(80, color='g')

        # plot KALMAN
        ax7 = plt.subplot2grid((unit, 4), (10, 0), rowspan=5, colspan=4, sharex=ax1)
#         ax7.plot(date, mean - closep, color='b')

        ax7.plot(date, mean, color='b')
        ax7.plot(date, mean + 2 * np.sqrt(var), color='y')
        ax7.plot(date, mean - 2 * np.sqrt(var), color='m')
        ax7.plot(date[-SP_MA:], Av2[-SP_MA:], 'r')
        
        for i in range(len(modified_data)):
            if(math.isnan(modified_data[i])):
                modified_data[i]=-1
            else: 
                modified_data[i]=1
        #ax7.plot(date, np.sqrt(var), color='m')
        #ax7.plot(date, modified_data, color='m')
        
        
        for label in ax1.xaxis.get_ticklabels():
            label.set_rotation(45)

        plt.subplots_adjust(left=0.1, bottom=0.2, right=0.9, top=0.9, wspace=0.1, hspace=0.1)

        plt.setp(ax1.get_xticklabels(), visible=False)

        plt.xlabel('Date')
        plt.suptitle(stock)
        plt.show()
        fig.savefig('plot.png')

    except Exception, e:
        print 'main loop', str(e)

def MA(values, window):
    weights = np.repeat(1.0, window) / window
    smas = np.convolve(values, weights, 'valid')
    return smas

def EMA(values, window):
    weights = np.exp(np.linspace(-1.0, 0.0, window))
    weights /= weights.sum()
    a = np.convolve(values, weights, mode='full')[:len(values)]
    a[:window] = a[window]
    return a

def MACD(x, slow=26, fast=12, window=9):
    emaslow = EMA(x, slow)
    emafast = EMA(x, fast)
    diff = emafast - emaslow
    dea = EMA(diff, window)
    osc = diff - dea
    return diff, dea, osc

def BOLL(x, window=20):
    mu = MA(x, window)
    diff = (x[window - 1:] - mu) ** 2
    weights = np.repeat(1.0, window) / window
    std = np.sqrt(np.convolve(diff, weights, 'valid'))

    mu = mu[window - 1:]
    up = mu + 2 * std
    down = mu - 2 * std
    return mu, up, down

def RSI(values, n=14):
    deltas = np.diff(values)
    seed = deltas[:n + 1]
    up = seed[seed >= 0].sum() / n
    down = -seed[seed < 0].sum() / n

    rs = up / down
    rsi = np.zeros_like(values)
    rsi[:n] = 100.0 - 100.0 / (1.0 + rs)

    for i in range(n, len(values)):
        delta = deltas[i - 1]
        if delta > 0:
            upval = delta
            downval = 0.0
        else:
            upval = 0.0
            downval = -delta

        up = (up * (n - 1) + upval) / n
        down = (down * (n - 1) + downval) / n

        rs = up / down
        rsi[i] = 100.0 - 100.0 / (1.0 + rs)

    return rsi

def KDJ(highp, lowp, closep, window=9):

    K = 50.0
    D = 50.0
    klist = []
    dlist = []
    jlist = []
    for i in range(window, len(closep)):
        highmax = max(highp[i - window:i])
        lowmin = min(lowp[i - window:i])
        rsv = (closep[i] - lowmin) / (highmax - lowmin) * 100
        K = (2.0 * K + rsv) / 3.0
        D = (2.0 * D + K) / 3.0
        J = 3.0 * D - 2.0 * K

        klist.append(K)
        dlist.append(D)
        jlist.append(J)
    return klist, dlist, jlist

def KALMAN(closep, volume, delta_power=0.01):
    
    T_max=max(volume)
        
    delta = 10**(-delta_power)
    Vw = delta / (1 - delta)
    
    x = np.zeros(len(closep))
    R = np.zeros(len(closep))
    Q = np.zeros(len(closep))
    H = 1
    
    # mask a portion of data
    ratio=0.5
    for i in range(1, len(closep)):
        if uniform(0,1)< ratio:
            closep[i]=ma.masked
    
    for i in range(len(closep)):
        y = closep[i]
        
        # update Kalman filter with latest price
        if i!=0 :
            x[i]=x[i-1]
            R[i]= R[i-1] + Vw
        else:
            x[i]=closep[0]
            R[i] = 0
        
        Ve = R[i]*((1-volume[i]/T_max))+0.1
        
        yhat = H*x[i]
        Q[i] = H*R[i]*H + Ve
        
        if not math.isnan(y):
            e = y - yhat
            K = R[i] / Q[i]
            x[i] = x[i] + K * e
            R[i] = R[i] - K *H* R[i]
    
    print R
    print x   
    print Q
        
    return x, Q,closep

    
